from Code.classes.subscriber import *
from Code.classes.subgroup import *
from Code.classes.sms import *
from Code.classes.client import *
from Code.classes.report import *
from Code.classes.campaign import *
from Code.sources.errs import *
import sqlite3
import os
from pathlib import Path


class AbstractData:
    def check_user(self, login, passwd):
        pass

    def get_user_by_fio(self, fio):
        pass

    def add_user(self, fio, login, passwd, roles):
        pass

    def del_user(self, user_id):
        pass

    def change_roles_for_user(self, user_id, new_roles):
        pass

    def add_client(self, client):
        pass

    def update_client(self, client_id, client):
        pass

    def get_client_id_by_name(self, client_name):
        pass

    def get_client_by_id(self, client_id):
        pass

    def get_all_subgroups(self):  # get it's names and id-s
        pass

    def get_subgroup_by_id(self, subgr_id):
        pass

    def get_subgroup_id_by_name(self, subgr_name):
        pass

    def add_subgroup(self, subgroup):
        pass

    def update_subgroup(self, subgr_id, subgr_subs):
        pass

    def flush_storage(self):
        pass

class Gateway(AbstractData):
    def __init__(self):
        self.users = []
        self.clients = [] # Consists of client names
        self.reports = [] # Client_id, first date, second date, report text
        self.campaigns = [] # Client_id, camp_id, start date, end date, status
        self.smss = [] # Client_id, camp_id, text, time, subgr_id
        self.subgroups = [] # Subgroup name
        self.subscribers = [] # Subgr_id, sub name, sub number

    # Functions for admin
    def check_user(self, login, passwd):
        for user in self.users:
            if (login == user[2] and passwd == user[3]): #if login and password are similar to tuple ones
                return user[4]  #returns role from tuple
        else:
            return -1

    def get_user_by_fio(self, fio):
        found_users = []
        for user in self.users:
            if (fio == user[1]):
                temp_list = [user[0],  user[1], user[2], user[4]]
                found_users.append(temp_list)
        return found_users #returns id, fio, login and role of found user (might include several users)

    def add_user(self, fio, login, passwd, roles):
        if (len(self.users) == 0):
            new_id = 0
        else:
            maxi = 0
            for i in range (0, len(self.users)):
                if (self.users[i][0] > maxi):
                    maxi = self.users[i][0]
            new_id = maxi + 1
        user_list = [new_id, fio, login, passwd, roles] #length is the actual id
        self.users.append(user_list)

    def del_user(self, user_id):
        row_id = -1
        for i in range (0, len(self.users)):
            if (user_id == self.users[i][0]):
                row_id = i
        if (row_id == -1):
            return -1
        else:
            self.users.pop(row_id)

    def change_roles_for_user(self, user_id, new_roles):
        for user in self.users:
            if (user_id == user[0]):
                user[4] = new_roles
                return 0 #OK
            else:
                return -1 #no such user

    # Functions for client admin

    def add_client(self, client_name): # Just add client name
        client_id = len(self.clients)
        self.clients.append(client_name)
        return client_id # Returns id of the new client

    '''
    def update_client(self, client_id, client_reps, client_camps, client_smss): # Full client update
        for rep in self.reports:
            if (rep[0] == client_id):
                self.reports.remove(rep)
        for camp in self.campaigns:
            if (camp[0] == client_id):
                self.campaigns.remove(camp)
        for sms in self.smss:
            if (sms[0] == client_id):
                self.smss.remove(sms)
        self.reports.extend(client_reps)
        self.campaigns.extend(client_camps)
        self.smss.extend(client_smss)
'''
    def update_client(self, client_id, client):
        for rep in self.reports:
            if (rep[0] == client_id):
                self.reports.remove(rep)
        for camp in self.campaigns:
            if (camp[0] == client_id):
                self.campaigns.remove(camp)
        for sms in self.smss:
            if (sms[0] == client_id):
                self.smss.remove(sms)
        for report in client.reports:
            rep = [client_id, report.first, report.second, report.report_text]
            self.reports.append(rep)
        for camp_number, campaign in enumerate(client.campaigns):
            camp = [client_id, camp_number, campaign.start_date, campaign.end_date, campaign.status]
            self.campaigns.append(camp)
            for sms in campaign.sms_s:
                sms_list = [client_id, camp_number, sms.sms_text,
                            sms.sms_time, self.subgroups.index(sms.group.name)]
                self.smss.append(sms_list)
        self.clients[client_id] = client.client_name

    def get_client_id_by_name(self, name):
        return self.clients.index(name) # Returns just client id

    # Returns client_name, list of client's reports, list of client's campaigns, list of campaign's smss
    # For each campaign
    def get_client_by_id(self, client_id):
        client_reps = []
        for rep in self.reports:
            if (rep[0] == client_id):
                client_reps.append(rep)
        client_camps = []
        for camp in self.campaigns:
            if (camp[0] == client_id):
                client_camps.append(camp)
        client_smss = []
        for sms in self.smss:
            if (sms[0] == client_id):
                client_smss.append(sms)
        return self.clients[client_id], client_reps, client_camps, client_smss

    # Functions for sms admin

    def get_all_subgroups(self):
        subgroups = []
        for subgr_number, subgr_name in enumerate(self.subgroups):
            list = [subgr_number, subgr_name]
            subgroups.append(list)
        return subgroups

    def get_subgroup_by_id(self, subgr_id): # Returns subgroup's name and subscribers
        subgr_subs = []
        for sub in self.subscribers:
            if (sub[0] == subgr_id):
                subgr_subs.append(sub)
        return self.subgroups[subgr_id], subgr_subs

    def get_subgroup_id_by_name(self, subgr_name):
        return self.subgroups.index(subgr_name)

    # Functions for subs admin

    def add_subgroup(self, subgr_name):
        #subgr_id = len(self.subgroups)
        self.subgroups.append(subgr_name)
        subgr_id = self.subgroups.index(subgr_name)
        return subgr_id  # Returns id of the new subgroup

    def update_subgroup(self, subgr_id, subgroup):
        list = []
        for sub in self.subscribers:
            if (sub[0] != subgr_id):
                list.append(sub)
        self.subscribers = []
        self.subscribers.extend(list)
        for sub in subgroup.subs:
            sub_row = [subgr_id, sub.name, sub.number]
            self.subscribers.append(sub_row)

    def flush_storage(self):
        del self.clients[:]
        del self.campaigns[:]
        del self.subgroups[:]
        del self.subscribers[:]
        del self.smss[:]
        del self.reports[:]
        del self.users[:]

class SQLiteGateway(AbstractData):
    def __init__(self, db_path, create_db=False):
        self._db_path = db_path

        # If you want create database file and add all necessary tables
        if create_db is True:
            self._create_db()

    def _create_db(self):
        # Delete database if exists
        database = Path(self._db_path)
        if database.exists():
            os.remove(self._db_path)

        connection = sqlite3.connect(self._db_path)
        cursor = connection.cursor()

        sql_command = """
                CREATE TABLE subgroup (
                    subgr_id INTEGER PRIMARY KEY,
                    name TEXT);"""
        cursor.execute(sql_command)

        sql_command = """
                CREATE TABLE subscriber (
                    subgr_id INTEGER,
                    sub_id INTEGER,
                    name TEXT,
                    number TEXT,
                    PRIMARY KEY (subgr_id, sub_id));"""
        cursor.execute(sql_command)

        sql_command = """
                CREATE TABLE client (
                    client_id INTEGER PRIMARY KEY,
                    name TEXT);"""
        cursor.execute(sql_command)

        sql_command = """
                CREATE TABLE campaign (
                    client_id INTEGER,
                    camp_id INTEGER,
                    start_date TEXT,
                    end_date TEXT,
                    status TEXT,
                    PRIMARY KEY (client_id, camp_id));"""
        cursor.execute(sql_command)

        sql_command = """
                CREATE TABLE sms (
                    client_id INTEGER,
                    camp_id INTEGER,
                    sms_id INTEGER,
                    sms_text TEXT,
                    sms_time TEXT,
                    subgr_id INTEGER,
                    PRIMARY KEY (client_id, camp_id, sms_id));"""
        cursor.execute(sql_command)

        sql_command = """
                CREATE TABLE report (
                    client_id INTEGER,
                    rep_id INTEGER,
                    first TEXT,
                    second TEXT,
                    report_text TEXT,
                    PRIMARY KEY (client_id, rep_id));"""
        cursor.execute(sql_command)

        sql_command = """
                CREATE TABLE user (
                    user_id INTEGER PRIMARY KEY,
                    fio TEXT,
                    login TEXT,
                    passwd TEXT,
                    roles INTEGER);"""
        cursor.execute(sql_command)

        connection.commit()
        connection.close()

    def check_user(self, login, passwd):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT roles FROM user WHERE login = :login AND passwd = :passwd;",
                    {'login': login, 'passwd': passwd})
        result = cur.fetchone()

        if result is None:
            print("User doesn't exist")
        else:
            result = int(str(result)[1])

        return result

    def get_user_by_fio(self, fio): #returns id, fio, login and role of found user (might include several users)
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT user_id, fio, login, roles FROM user WHERE fio = :fio;", {'fio': fio})
        result = cur.fetchone()
        if result is None:
            print("User doesn't exist")

        connection.commit()
        connection.close()
        return result

    def add_user(self, fio, login, passwd, roles):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        sql_command = """
                            INSERT INTO user (fio, login, passwd, roles)
                            VALUES (:fio, :login, :passwd, :roles);"""
        cur.execute(sql_command, {'fio': fio,
                                  'login': login,
                                  'passwd': passwd,
                                  'roles': roles})

        connection.commit()
        connection.close()

    def del_user(self, user_id):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT user_id FROM user WHERE user_id = :user_id;", {'user_id': user_id})
        result = cur.fetchone()
        if result is None:
            print("User with such id doesn't exist")

        cur.execute("DELETE FROM user WHERE user_id = :user_id;", {'user_id': user_id})

        connection.commit()
        connection.close()

    def change_roles_for_user(self, user_id, new_roles):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT user_id FROM user WHERE user_id = :user_id;", {'user_id': user_id})
        result = cur.fetchone()
        if result is None:
            print("User with such id doesn't exist")

        sql_command = """
                        UPDATE user
                        SET roles = :roles
                        WHERE user_id = :user_id;"""
        cur.execute(sql_command, {'roles': new_roles,
                                  'user_id': user_id})

        connection.commit()
        connection.close()

    def get_client_id_by_name(self, name): #returns client_id
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT client_id FROM client WHERE name = :name;", {'name': name})
        result = cur.fetchone()
        if result is None:
            #raise LogicError('Err')
            print("Client with such id doesn't exist")
        else:
            result = int(str(result)[1])

        connection.commit()
        connection.close()
        return result

    def add_client(self, client_name): #returns id of the new client
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        sql_command = """
                                    INSERT INTO client (name)
                                    VALUES (:name);"""
        cur.execute(sql_command, {'name': client_name})

        cur.execute("SELECT client_id FROM client WHERE name = :name;", {'name': client_name})
        client_id = cur.fetchone()

        connection.commit()
        connection.close()
        return client_id

    def update_client(self, client_id, client):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT client_id FROM client WHERE client_id = :client_id;", {'client_id': client_id})
        result = cur.fetchone()
        if result is None:
            print("Client with such id doesn't exist")

        cur.execute("DELETE FROM client WHERE client_id = :client_id;", {'client_id': client_id})
        cur.execute("DELETE FROM report WHERE client_id = :client_id;", {'client_id': client_id})
        cur.execute("DELETE FROM campaign WHERE client_id = :client_id;", {'client_id': client_id})
        cur.execute("DELETE FROM sms WHERE client_id = :client_id;", {'client_id': client_id})

        sql_command = """
                                            INSERT INTO client (name)
                                            VALUES (:name);"""
        cur.execute(sql_command, {'name': client.client_name})

        cur.execute("SELECT client_id FROM client WHERE name = :name;", {'name': client.client_name})
        client_id = cur.fetchone()[0]

        # insert reports
        for rep_number, report in enumerate(client.reports):
            sql_command = """
                                    INSERT INTO report (client_id, rep_id, first, second, report_text)
                                    VALUES (:client_id, :rep_id, :first, :second, :report_text);"""
            cur.execute(sql_command, {'client_id': client_id,
                                      'rep_id': rep_number,
                                      'first': report.start_time(),
                                      'second': report.end_time(),
                                      'report_text': report.report_text})

        # insert campaigns
        for camp_number, campaign in enumerate(client.campaigns):
            sql_command = """
                                    INSERT INTO campaign (client_id, camp_id, start_date, end_date, status)
                                    VALUES (:client_id, :camp_id, :start_date, :end_date, :status);"""
            cur.execute(sql_command, {'client_id': client_id,
                                      'camp_id': camp_number,
                                      'start_date': campaign.start_date,
                                      'end_date': campaign.end_date,
                                      'status': campaign.status})

        # insert sms_s
        for camp_number, campaign in enumerate(client.campaigns):
            for sms_number, sms in enumerate(campaign.sms_s):
                cur.execute("SELECT subgr_id FROM subgroup WHERE name = :name;", {'name': sms.group.name})
                subgr_id = cur.fetchone()

                subgr_id = int(str(subgr_id)[1])
                print subgr_id
                sql_command = """
                                        INSERT INTO sms (client_id, camp_id, sms_id, sms_text, sms_time, subgr_id)
                                        VALUES (:client_id, :camp_id, :sms_id, :sms_text, :sms_time, :subgr_id);"""
                cur.execute(sql_command, {'client_id': client_id,
                                          'camp_id': camp_number,
                                          'sms_id': sms_number,
                                          'sms_text': sms.sms_text,
                                          'sms_time': sms.sms_time,
                                          'subgr_id': subgr_id})

        connection.commit()
        connection.close()

    def get_client_by_id(self, client_id):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT name FROM client WHERE client_id = :client_id;", {'client_id': int(client_id)})
        result = cur.fetchone()

        if result is None:
            print("Client with such id doesn't exist")
        else:
            result = str(result[0])

        client_name = result

        # Get all reports
        cur.execute("SELECT client_id, first, second, report_text FROM report WHERE client_id = :client_id;", {'client_id': client_id})
        client_reps = cur.fetchall()

        # Get all campaigns
        cur.execute("SELECT client_id, camp_id, start_date, end_date, status FROM campaign WHERE client_id = :client_id;", {'client_id': client_id})
        client_camps = cur.fetchall()

        # Get all smss
        cur.execute("SELECT client_id, camp_id, sms_text, sms_time, subgr_id FROM sms WHERE client_id = :client_id;", {'client_id': client_id})
        client_smss = cur.fetchall()

        connection.commit()
        connection.close()
        return client_name, client_reps, client_camps, client_smss

    def get_all_subgroups(self):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT subgr_id, name FROM subgroup;")
        subgroups = cur.fetchall()

        connection.commit()
        connection.close()
        return subgroups

    def get_subgroup_by_id(self, subgr_id): # Returns subgroup's name and subscribers
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT subgr_id FROM subgroup WHERE subgr_id = :subgr_id;", {'subgr_id': subgr_id})
        result = cur.fetchone()
        if result is None:
            print("Subgroup with such id doesn't exist")
        else:
            result = str(result[0])

        cur.execute("SELECT subgr_id, name FROM subgroup WHERE subgr_id = :subgr_id;", {'subgr_id': subgr_id})
        name = cur.fetchall()
        name = name[0][1]

        cur.execute("SELECT subgr_id, name, number FROM subscriber WHERE subgr_id = :subgr_id;", {'subgr_id': subgr_id})
        subgr_subs = cur.fetchall()

        connection.commit()
        connection.close()
        return name, subgr_subs

    def get_subgroup_id_by_name(self, subgr_name):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT * FROM subgroup WHERE name = :name;", {'name': subgr_name})
        result = cur.fetchone()
        if result is None:
            print("Subgroup with such name doesn't exist")

        cur.execute("SELECT subgr_id FROM subgroup WHERE name = :name;", {'name': subgr_name})
        subgr_id = cur.fetchone()

        connection.commit()
        connection.close()
        return subgr_id

    def add_subgroup(self, subgr_name): # Returns id of the new subgroup
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT * FROM subgroup WHERE name = :name;", {'name': subgr_name})
        result = cur.fetchone()
        if result is None:
            print("Subgroup with such name doesn't exist")

        sql_command = """
                        INSERT INTO subgroup (name)
                        VALUES (:name);"""
        cur.execute(sql_command, {'name': subgr_name})

        connection.commit()
        connection.close()

    def update_subgroup(self, subgr_id, subgroup):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("SELECT subgr_id FROM subgroup WHERE subgr_id = :subgr_id;", {'subgr_id': subgr_id})
        result = cur.fetchone()
        if result is None:
            print("Subgroup with such id doesn't exist")

        cur.execute("DELETE FROM subscriber WHERE subgr_id = :subgr_id;", {'subgr_id': subgr_id})

        for sub_number, sub in enumerate(subgroup.subs):
            sql_command = """
                            INSERT INTO subscriber (subgr_id, sub_id, name, number)
                            VALUES (:subgr_id, :sub_id, :name, :number);"""
            cur.execute(sql_command, {'subgr_id': subgr_id,
                                      'sub_id': sub_number,
                                      'name': sub.name,
                                      'number': sub.number})

        connection.commit()
        connection.close()

    def flush_storage(self):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        cur.execute("DELETE FROM user;")
        cur.execute("DELETE FROM client;")
        cur.execute("DELETE FROM report;")
        cur.execute("DELETE FROM campaign;")
        cur.execute("DELETE FROM sms;")
        cur.execute("DELETE FROM subgroup;")
        cur.execute("DELETE FROM subscriber;")

        connection.commit()
        connection.close()