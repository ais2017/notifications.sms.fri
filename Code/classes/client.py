from campaign import *
from report import *

class Client:
    def __init__(self, client_name):
        if not isinstance(client_name, str):
            raise TypeError
        self.client_name = client_name
        self.campaigns = list()
        self.reports = list()

    def change_name(self, client_name):
        if not isinstance(client_name, str):
            raise TypeError
        self.client_name = client_name

    def add_campaign(self, camp):
        if not isinstance(camp, Campaign):
            raise TypeError
        self.campaigns.append(camp)

    def del_campaign(self, camp):
        if not isinstance(camp, Campaign):
            raise TypeError
        self.campaigns.remove(camp)

    def get_campaign(self, camp_num):
        if not isinstance(camp_num, int):
            raise TypeError
        return self.campaigns[camp_num]

    def add_report(self, rep):
        self.reports.append(rep)

    def del_report(self, rep):
        self.reports.remove(rep)

    def get_report(self, rep_num):
        if not isinstance(rep_num, int):
            raise TypeError
        return self.reports[rep_num]

    def print_campaigns(self):
        text = ''
        for i in range(0, len(self.campaigns)):
            text = text + str(i+1) + ' campaign: started at ' + self.campaigns[i].start_date + ' ; number of sms: ' + str(len(self.campaigns[i].sms_s)) + '\n'
        return text