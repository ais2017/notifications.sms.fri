from subscriber import *

class Subgroup:
    def __init__(self, name):
       # if not isinstance(name, str):
        #    raise TypeError
        self.name = name
        self.subs = list()

    def add_subscriber(self, sub):
        if not isinstance(sub, Subscriber):
            raise TypeError
        self.subs.append(sub)

    def del_subscriber(self, sub):
        if not isinstance(sub, Subscriber):
            raise TypeError
        self.subs.remove(sub)

    def change_name(self, new_name):
        if not isinstance(new_name, str):
            raise TypeError
        self.name = new_name

'''    def get_sub(self, number):
        if not isinstance(number, int):
            raise TypeError
        for sub in self.subs:
            if (sub.get_number() == number):
                return sub'''