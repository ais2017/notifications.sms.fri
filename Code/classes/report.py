from Code.classes.client import *
from Code.sources.errs import *

class Report:
    def rep_make(self):
        count = 0
        for camp in self.cl.campaigns:
            if ((str(self.first) < str(camp.start_date) and str(self.second) > str(camp.start_date)) or
                    (str(self.second) > str(camp.end_date) and str(self.first) < str(camp.end_date))):
                count += 1
        return ('During the period from ' + self.first + ' til ' + self.second + ' client ' + self.cl.client_name + ' had '
                + str(count) + ' active campaigns')

    def __init__(self, first, second, client, report_text=0):
        if not isinstance(first, str):
            raise TypeError
        if not isinstance(second, str):
            raise TypeError
        if self.check_date(first) != 0:
            raise LogicError('Wrong type of date')
        if self.check_date(second) != 0:
            raise LogicError('Wrong type of date')
        self.first = first
        self.second = second
        self.cl = client
        if (report_text == 0):
            self.report_text = self.rep_make()
        else:
            self.report_text = report_text

    def check_date(self, date):
        check_list = []
        i = 0
        if len(date) != 10:
            i += 1
        check_list.append(str(date).split('-'))
        for num, el in enumerate(check_list):
            if num == 1 and (len(el) != 4 or int(el) < 2017):
                i += 1
            elif num == 2 and (len(el) != 2 or el > 31 or el < 01):
                i += 1
            elif num == 3 and (len(el) != 2 or el > 31 or el < 01):
                i += 1
            elif num > 3:
                i += 1
        return i

    def start_time(self):
        return self.first

    def end_time(self):
        return self.second