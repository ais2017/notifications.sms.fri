from subgroup import *
from Code.sources.errs import *

#add counter
class Sms:
    def __init__(self, sms_text='0', sms_time='0', group=Subgroup('Default')):
        if not isinstance(sms_text, str):
            raise TypeError
        if not isinstance(sms_time, str):
            raise TypeError
        if not isinstance(group, Subgroup):
            raise TypeError
        if self.check_date(sms_time) != 0:
            raise LogicError('Wrong type of date')
        self.sms_text = sms_text
        self.sms_time = sms_time
        self.group = group

    def check_date(self, date):
        check_list = []
        i = 0
        if len(date) != 10:
            i += 1
        check_list.append(str(date).split('-'))
        for num, el in enumerate(check_list):
            if num == 1 and (len(el) != 4 or int(el) < 2017):
                i += 1
            elif num == 2 and (len(el) != 2 or el > 31 or el < 01):
                i += 1
            elif num == 3 and (len(el) != 2 or el > 31 or el < 01):
                i += 1
            elif num > 3:
                i += 1
        return i

    def change_text(self, new_text):
        if not isinstance(new_text, str):
            raise TypeError
        self.sms_text = new_text

    def change_time(self, new_time):
        if not isinstance(new_time, str):
            raise TypeError
        if self.check_date(new_time) != 0:
            raise LogicError('Wrong type of date')
        self.sms_time = new_time

    def get_text(self):
        return self.sms_text

    def get_time(self):
        return self.sms_time

    def get_group(self):
        return self.group

    def get_send_info(self):
        sms_subs = []
        for subscriber in self.group.subs:
            sms_subs.append(subscriber.number)
        return self.sms_text, self.sms_time, sms_subs
