from datetime import datetime
from sms import *
from Code.sources.errs import *

class Campaign:
    def __init__(self, start_date=str(datetime.now())[:10], end_date=0, status=True):
        if self.check_date(start_date) != 0:
            raise LogicError('Wrong type of date')
        self.start_date = start_date
        self.end_date = end_date
        self.status = status
        self.sms_s = list()

    def add_sms(self, sms):
        if not isinstance(sms, Sms):
            raise TypeError
        #if (self.status == True):
        self.sms_s.append(sms)

    def del_sms(self, sms):
        if not isinstance(sms, Sms):
            raise TypeError
        if (self.status == True):
            self.sms_s.remove(sms)

    def close_campaign(self):
        self.status = False
        self.end_date = str(datetime.now())[:10]

    def get_sms(self, sms_num):#test!!!
        if not isinstance(sms_num, int):
            raise TypeError
        return self.sms_s[sms_num]

    def check_date(self, date):
        check_list = []
        i = 0
        if len(date) != 10:
            i += 1
        check_list.append(str(date).split('-'))
        for num, el in enumerate(check_list):
            if num == 1 and (len(el) != 4 or int(el) < 2017):
                i += 1
            elif num == 2 and (len(el) != 2 or el > 31 or el < 01):
                i += 1
            elif num == 3 and (len(el) != 2 or el > 31 or el < 01):
                i += 1
            elif num > 3:
                i += 1
        return i

    '''def print_smss(self):
        text = ''
        for i in range(0, len(self.sms_s)):
            text = text + str(i+1) + ' sms: ' + self.sms_s[i].get_text() + ' \n\n; send time: ' + str(self.sms_s[i].get_time()) + '\n\n'
        return text'''