class Subscriber:
    def __init__(self, name, number):
        if not isinstance(name, str):
            raise TypeError
        if not isinstance(number, str):
            raise TypeError
        self.name = name
        self.number = number

    def change_name(self, name):
        if not isinstance(name, str):
            raise TypeError
        self.name = name

    def change_number(self, number):
        if not isinstance(number, str):
            raise TypeError
        self.number = number

    def get_name(self):
        return self.name

    def get_number(self):
        return self.number
