# -*- coding: utf-8 -*-

import config
import telebot
from telebot import types
from Code.use_cases.Use_case import *

bot = telebot.TeleBot(config.token)
uc = UseCases(SQLiteGateway('/Users/kirilltitov/study/ais/Code/database/database.db'))

user_data = []
user_props = [] # For managing data in all objects except users

@bot.message_handler(commands=["start"])
def cmd_start(message):
    del user_data[:]
    del user_props[:]
    sent = bot.send_message(message.chat.id, "Добро пожаловать в телеграмм бот программы Notification Sms!\nВведи свой логин")
    bot.register_next_step_handler(sent, print_login)

def print_login(message):
    user_data.append(message.text)
    sent = bot.send_message(message.chat.id, 'Привет, {login}. Введи свой пароль'.format(login=message.text))
    bot.register_next_step_handler(sent, print_password)

def print_password(message):
    user_data.append(message.text)
    role_check = uc.check_user(user_data[0], user_data[1])
    sent = bot.send_message(message.chat.id, 'Твои права в системе: {roles}'.format(roles=role_check))
    if (role_check == 'ADMIN'):
        sent = bot.send_message(message.chat.id, 'Введи ФИО пользователя')
        bot.register_next_step_handler(sent, get_user_fio)
    elif (role_check == 'CLIENT_ADM'):
        sent = bot.send_message(message.chat.id, 'Введи название компании (клиента)')
        bot.register_next_step_handler(sent, get_client_company)
    elif (role_check == 'SMS_ADM'):
        sent = bot.send_message(message.chat.id, 'Введи название компании (клиента)')
        bot.register_next_step_handler(sent, get_client_company_for_sms)
    elif (role_check == 'SUB_ADM'):
        bot.send_message(message.chat.id, 'Доступные группы подписчиков: {subgrs}'.format(subgrs=uc.get_all_subgroups()))
        sent = bot.send_message(message.chat.id, 'Введи id нужной группы подписчиков')
        bot.register_next_step_handler(sent, subgr_choose_subgroup)
    else:
        pass

# Continue or Exit

def user_continue_exit(message):
    if message.text != 'Exit':
        del user_data[:]
        del user_props[:]
        sent = bot.send_message(message.chat.id, 'Введи ФИО пользователя')
        bot.register_next_step_handler(sent, get_user_fio)
    elif message.text == 'Exit':
        sent = bot.send_message(message.chat.id, 'Для перехода на страницу авторизации отправь любое сообщение')
        bot.register_next_step_handler(sent, cmd_start)

def client_continue_exit(message):
    if message.text != 'Exit':
        del user_data[:]
        del user_props[:]
        sent = bot.send_message(message.chat.id, 'Введи название компании (клиента)')
        bot.register_next_step_handler(sent, get_client_company)
    elif message.text == 'Exit':
        sent = bot.send_message(message.chat.id, 'Для перехода на страницу авторизации отправь любое сообщение')
        bot.register_next_step_handler(sent, cmd_start)

def sms_continue_exit(message):
    if message.text == 'Continue':
        del user_data[:]
        del user_props[:]
        sent = bot.send_message(message.chat.id, 'Введи название компании (клиента)')
        bot.register_next_step_handler(sent, get_client_company_for_sms)
    elif message.text == 'Exit':
        sent = bot.send_message(message.chat.id, 'Для перехода на страницу авторизации отправь любое сообщение')
        bot.register_next_step_handler(sent, cmd_start)

def subgr_continue_exit(message):
    if message.text == 'Continue':
        del user_data[:]
        del user_props[:]
        bot.send_message(message.chat.id, 'Доступные группы подписчиков: {subgrs}'.format(subgrs=uc.get_all_subgroups()))
        sent = bot.send_message(message.chat.id, 'Введи id нужной группы подписчиков')
        bot.register_next_step_handler(sent, subgr_choose_subgroup)
    elif message.text == 'Exit':
        sent = bot.send_message(message.chat.id, 'Для перехода на страницу авторизации отправь любое сообщение')
        bot.register_next_step_handler(sent, cmd_start)

# Functions for User admin

def get_user_fio(message):
    if uc.get_user_by_fio(message.text) is None:
        user_props.append(message.text)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Yes', 'No')
        sent = bot.reply_to(message, 'Нет такого пользователя. Создать нового?', reply_markup=markup)
        bot.register_next_step_handler(sent, user_create_or_not)
    else:
        user_props.append(message.text)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('New Roles', 'Delete User', 'None')
        sent = bot.reply_to(message, 'Пользователь обнаружен. Что с ним сделать?', reply_markup=markup)
        bot.register_next_step_handler(sent, user_change_or_delete)

def user_create_or_not(message):
    if str(message.text) == 'Yes':
        sent = bot.send_message(message.chat.id, 'Введи логин, пароль и роль через запятую')
        bot.register_next_step_handler(sent, create_user)
    if str(message.text) == 'No':
        #bot.send_message(message.chat.id, 'Отмена создания пользователя')
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Отмена создания пользоваеля. Продолжить или выйти?', reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    if str(message.text) != 'Yes' and str(message.text) == 'No':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue')
        sent = bot.send_message(message.chat.id, 'Что-то пошло не так. Жми на кнопку.',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)

def create_user(message):
    user_props.extend(str(message.text).split(','))
    uc.add_user(user_props[0], user_props[1], user_props[2], int(user_props[3]))
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Пользователь успешно добавлен. Продолжить или выйти?',
                            reply_markup=markup)
    bot.register_next_step_handler(sent, user_continue_exit)

def user_change_or_delete(message):
    if str(message.text) == 'New Roles':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Admin', 'Client User', 'Sms User', 'Subscriber User')
        sent = bot.send_message(message.chat.id, 'Выбери новую роль пользователя', reply_markup=markup)
        bot.register_next_step_handler(sent, change_user_roles)
    elif str(message.text) == 'Delete User':
        user_id = uc.get_user_by_fio(user_props[0])[0]
        uc.del_user(user_id)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Пользователь {name} успешно удален. Продолжить или выйти?'.format(name=user_props[0]),
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    elif str(message.text) == 'None':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Отмена действия над пользователем. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    else:
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue')
        sent = bot.send_message(message.chat.id, 'Что-то пошло не так. Жми на кнопку.',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)

def change_user_roles(message):
    if str(message.text) == 'Admin':
        user_id = uc.get_user_by_fio(user_props[0])[0]
        uc.change_roles_for_user(user_id, 1)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Пользовательская роль успешно обновлена. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    if str(message.text) == 'Client User':
        user_id = uc.get_user_by_fio(user_props[0])[0]
        uc.change_roles_for_user(user_id, 2)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Пользовательская роль успешно обновлена. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    if str(message.text) == 'Sms User':
        user_id = uc.get_user_by_fio(user_props[0])[0]
        uc.change_roles_for_user(user_id, 3)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Пользовательская роль успешно обновлена. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    if str(message.text) == 'Subscriber User':
        user_id = uc.get_user_by_fio(user_props[0])[0]
        uc.change_roles_for_user(user_id, 4)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Пользовательская роль успешно обновлена. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)
    else:
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue')
        sent = bot.send_message(message.chat.id, 'Что-то пошло не так. Жми на кнопку.',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, user_continue_exit)

# Functions for client admin

def get_client_company(message):
    if uc.get_client(message.text) is None:
        user_props.append(message.text)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Yes', 'No')
        sent = bot.reply_to(message, 'Нет такой компании. Зарегистрировать новую?', reply_markup=markup)
        bot.register_next_step_handler(sent, client_new_or_not)
    else:
        user_props.append(message.text)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Change Client', 'Make Report', 'None')
        sent = bot.reply_to(message, 'Компания обнаружена. Что с ней сделать?', reply_markup=markup)
        bot.register_next_step_handler(sent, client_change_or_report)

def client_new_or_not(message):
    if str(message.text) == 'Yes':
        sent = bot.send_message(message.chat.id, 'Подтверди название компании еще раз')
        bot.register_next_step_handler(sent, create_client)
    if str(message.text) == 'No':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Отмена создания компании. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, client_continue_exit)
    else:
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue')
        sent = bot.send_message(message.chat.id, 'Что-то пошло не так. Жми на кнопку.',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, client_continue_exit)

def create_client(message):
    uc.new_client(str(message.text))
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Компания успешно добавлена. Продолжить или выйти?',
                            reply_markup=markup)
    bot.register_next_step_handler(sent, client_continue_exit)

def client_change_or_report(message):
    if str(message.text) == 'Change Client':
        client_id = uc.get_client_id(user_props[0]) # It was name here (in user props)
        del user_props[:]
        user_props.append(client_id) # Now it's id
        sent = bot.send_message(message.chat.id, 'Введи новое название компании')
        bot.register_next_step_handler(sent, client_change_name)
    if str(message.text) == 'Make Report':
        client_id = uc.get_client_id(user_props[0])  # It was name here (in user props)
        del user_props[:]
        user_props.append(client_id)  # Now it's id
        sent = bot.send_message(message.chat.id, 'Введи начальную и конечную даты через запятую в формате гггг-мм-дд')
        bot.register_next_step_handler(sent, client_make_report)
    if str(message.text) == 'None':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Отмена действия над клиентом. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, client_continue_exit)
    if (str(message.text) != 'Change Client' and str(message.text) != 'Make Report' and str(message.text) != 'None'):
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue')
        sent = bot.send_message(message.chat.id, 'Что-то пошло не так. Жми на кнопку.',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, client_continue_exit)

def client_change_name(message):
    uc.change_name_client(user_props[0], str(message.text))
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Название успешно изменено. Продолжить или выйти?',
                            reply_markup=markup)
    bot.register_next_step_handler(sent, client_continue_exit)

def client_make_report(message):
    user_props.extend(str(message.text).split(','))
    st_date = ''
    st_date = st_date + str(user_props[1])
    end_date = ''
    end_date = end_date + str(user_props[2])
    try:
        uc.new_report(st_date, end_date, int(user_props[0]))
    except LogicError:
        print "111111"
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Отчет успешно добавлен. Продолжить или выйти?',
                            reply_markup=markup)
    bot.register_next_step_handler(sent, client_continue_exit)

# Functions for sms admin

def get_client_company_for_sms(message):
    if uc.get_client(message.text) is None:
        user_props.append(message.text)
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Нет такой компании. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, sms_continue_exit)
    else:
        try:
            user_props.append(message.text) # Client name
        except LogicError:
            bot.send_message(message.chat.id, "QWQQQQQQQQ")
        client_id = uc.get_client_id(message.text)
        bot.send_message(message.chat.id, 'Доступны следующие кампании: {camps}'.format(camps=uc.get_campaigns_by_client_id(client_id)))
        sent = bot.send_message(message.chat.id, 'Введи id нужной кампании')
        bot.register_next_step_handler(sent, choose_campaign)
#!!!!! Add campaign

def choose_campaign(message):
    user_props.append(message.text) # Camp id
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Make Sms', 'Change Sms', 'Send Sms', 'None') # Should add send sms
    sent = bot.send_message(message.chat.id, 'Выбери действие с кампанией', reply_markup=markup)
    bot.register_next_step_handler(sent, campaign_make_change_send_sms)

def campaign_make_change_send_sms(message):
    if message.text == 'Make Sms':
        bot.send_message(message.chat.id, 'Доступны следующие группы подписчиков: {subgrs}'.format(subgrs=uc.get_all_subgroups()))
        sent = bot.send_message(message.chat.id, 'Введи id нужной группы подписчиков')
        bot.register_next_step_handler(sent, campaign_choose_subgroup)
    elif message.text == 'Change Sms':
        client_id = uc.get_client_id(user_props[0])
        if (len(uc.get_all_sms(int(client_id), int(user_props[1]))) == 0):
            markup = types.ReplyKeyboardMarkup(True, True)
            markup.row('Continue', 'Exit')
            sent = bot.send_message(message.chat.id, 'Нет доступных смс. Продолжить или выйти?',
                                    reply_markup=markup)
            bot.register_next_step_handler(sent, sms_continue_exit)
        else:
            #print uc.get_all_sms(int(client_id), int(user_props[1]))
            bot.send_message(message.chat.id, 'Доступны следующие смс: {smss}'.format(smss=uc.get_all_sms(int(client_id), int(user_props[1]))))
            sent = bot.send_message(message.chat.id, 'Введи id нужной sms')
            bot.register_next_step_handler(sent, campaign_choose_sms)
    elif message.text == 'Send Sms': # Writing now
        client_id = uc.get_client_id(user_props[0])
        if (len(uc.get_all_sms(int(client_id), int(user_props[1]))) == 0):
            markup = types.ReplyKeyboardMarkup(True, True)
            markup.row('Continue', 'Exit')
            sent = bot.send_message(message.chat.id, 'Нет доступных смс. Продолжить или выйти?',
                                    reply_markup=markup)
            bot.register_next_step_handler(sent, sms_continue_exit)
        else:
            # print uc.get_all_sms(int(client_id), int(user_props[1]))
            bot.send_message(message.chat.id, 'Доступны следующие смс: {smss}'.format(
                smss=uc.get_all_sms(int(client_id), int(user_props[1]))))
            sent = bot.send_message(message.chat.id, 'Введи id нужной sms')
            bot.register_next_step_handler(sent, campaign_choose_sms_for_send)
    elif message.text == 'None':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Окей. Продолжить или выйти?',
                                reply_markup=markup)
        bot.register_next_step_handler(sent, sms_continue_exit)

def campaign_choose_subgroup(message):
    user_props.append(message.text) # Subgr id
    sent = bot.send_message(message.chat.id, 'Введи текст и дату отправки смс через запятую')
    bot.register_next_step_handler(sent, campaign_create_sms)

def campaign_create_sms(message):
    user_props.extend(str(message.text).split(','))
    #print user_props
    client_id = uc.get_client_id(user_props[0])
    uc.new_sms(int(client_id), int(user_props[1]), str(user_props[3]), str(user_props[4]), int(user_props[2]))
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Сообщение добавлено. Продолжить или выйти?',
                            reply_markup=markup)
    bot.register_next_step_handler(sent, sms_continue_exit)

def campaign_choose_sms(message):
    user_props.append(message.text) # Sms id
    sent = bot.send_message(message.chat.id, 'Введи новый текст и новую дату отправки через запятую')
    bot.register_next_step_handler(sent, campaign_change_sms)

def campaign_choose_sms_for_send(message):
    user_props.append(message.text) # Sms id
    client_id = uc.get_client_id(user_props[0])
    text, time, subs = uc.send_sms(int(client_id), int(user_props[1]), int(user_props[2]))
    print text, time, subs
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Смс с текстом {s_text} отправлена подписчикам: {s_subs}. Продолжить или выйти?'.format(s_text=text, s_subs=subs), reply_markup=markup)
    bot.register_next_step_handler(sent, sms_continue_exit)

def campaign_change_sms(message):
    user_props.extend(str(message.text).split(','))
    client_id = uc.get_client_id(user_props[0])
    uc.change_sms(int(client_id), int(user_props[1]), int(user_props[2]), str(user_props[3]), str(user_props[4])) # Doesn't work
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Изменения сохранены. Продолжить или выйти?',
                            reply_markup=markup)
    bot.register_next_step_handler(sent, sms_continue_exit)

# Functions for sub admin

def subgr_choose_subgroup(message):
    print message.text
    user_props.append(message.text) # Subgr id
    sent = bot.send_message(message.chat.id, 'Введи ФИО подписчика')
    bot.register_next_step_handler(sent, subgr_find_sub)

def subgr_find_sub(message):
    #print uc.get_subgroup_subs(user_props[0])
    flag = 0
    for sub in uc.get_subgroup_subs(user_props[0]):
        if message.text == sub[1]:
            flag = 1
            user_props.append(sub[1]) # Name
            user_props.append(sub[2]) # Number
            bot.send_message(message.chat.id, 'Подписчик найден: {subscr}'.format(subscr=sub))
            markup = types.ReplyKeyboardMarkup(True, True)
            markup.row('Change', 'Delete')
            sent = bot.send_message(message.chat.id, 'Выбери действие над подписчиком', reply_markup=markup)
            bot.register_next_step_handler(sent, subgr_change_or_delete)
    if flag == 0:
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Yes', 'No')
        user_props.append(message.text) # Name
        sent = bot.send_message(message.chat.id, 'Такого подписчика нет. Создать?', reply_markup=markup)
        bot.register_next_step_handler(sent, subgr_add_or_not_sub)

def subgr_change_or_delete(message):
    if message.text == 'Change':
        sent = bot.send_message(message.chat.id, 'Введи новое ФИО и номер через запятую')
        bot.register_next_step_handler(sent, subgr_change_sub)
    elif message.text == 'Delete':
        #print user_props
        uc.delete_subscriber(int(user_props[0]), str(user_props[1]), str(user_props[2]))
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Подписчик успешно удален. Продолжить или выйти?', reply_markup=markup)
        bot.register_next_step_handler(sent, subgr_continue_exit)

def subgr_change_sub(message):
    user_props.extend(str(message.text).split(','))
    uc.change_subscriber(int(user_props[0]), str(user_props[1]), str(user_props[2]), str(user_props[3]), str(user_props[4]))
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Подписчик успешно изменен. Продолжить или выйти?', reply_markup=markup)
    bot.register_next_step_handler(sent, subgr_continue_exit)

def subgr_add_or_not_sub(message):
    if message.text == 'Yes':
        sent = bot.send_message(message.chat.id, 'Введи номер подписчика {sub}'.format(sub=user_props[1]))
        bot.register_next_step_handler(sent, subscr_add_sub)
    elif message.text == 'No':
        markup = types.ReplyKeyboardMarkup(True, True)
        markup.row('Continue', 'Exit')
        sent = bot.send_message(message.chat.id, 'Окей. Продолжить или выйти?', reply_markup=markup)
        bot.register_next_step_handler(sent, subgr_continue_exit)

def subscr_add_sub(message):
    uc.add_subscriber(int(user_props[0]), str(user_props[1]), str(message.text))
    markup = types.ReplyKeyboardMarkup(True, True)
    markup.row('Continue', 'Exit')
    sent = bot.send_message(message.chat.id, 'Подписчик успешно добавлен. Продолжить или выйти?', reply_markup=markup)
    bot.register_next_step_handler(sent, subgr_continue_exit)

if __name__ == '__main__':
     bot.polling(none_stop=True)

