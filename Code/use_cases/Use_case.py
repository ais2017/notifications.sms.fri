from Code.sources.gateway import *
from Code.sources.errs import *

#gateway = Gateway()
#gateway = SQLiteGateway('/Users/kirilltitov/study/ais/Code/database/database.db')

class UseCases:
    def __init__(self, gateway):
        if locals().get(gateway, False):
            raise LogicError("No such gateway")
        else:
            self.gateway = gateway


    '''def admins_to_tuple(admins):
        admins_tuple = []
        for admin in admins:
            admins_tuple.append((admin.id,
                                 admins.fio))
        return admins_tuple'''

    def clients_to_tuple(self, clients):
        clients_tuple = []
        for client in clients:
            clients_tuple.append(client.client_name)
        return  clients_tuple

    def campaigns_to_tuple(self, campaigns):
        campaigns_tuple = []
        camp_id = 0
        for camp_id, campaign in enumerate(campaigns):
            campaigns_tuple.append((camp_id, #campaign id in client's list
                                    campaign.start_date, #campaign start date
                                    campaign.end_date, #campaign end date
                                    len(campaign.sms_s))) #campaign number of smss
            camp_id += 1
        return campaigns_tuple

    def smss_to_tuple(self, campaign):
        smss_tuple = []
        sms_id = 0
        for sms in campaign.sms_s:
            smss_tuple.append((sms_id,
                               sms.sms_time,
                               sms.sms_text,
                               sms.group.name))
            sms_id += 1
        return smss_tuple

    '''def subgroups_to_tuple(subgroups):
        subgroups_tuple = []
        for subgroup in subgroups:
            subgroups_tuple.append(subgroup.name)
        return subgroups_tuple
    '''
    def subscribers_to_tuple(self, subscribers):
        subscribers_tuple = []
        for subscriber in subscribers:
            subscribers_tuple.append((subscriber.name,
                                      subscriber.number))
        return subscribers_tuple

    def assemble_client_from_rows(self, client_id):
        client_name, client_reps, client_camps, client_smss = self.gateway.get_client_by_id(client_id)
        #print client_name, client_reps, client_camps, client_smss
        if (client_name is None and client_reps is None and client_smss is None and client_camps is None):
            return None
        else:
            client = Client(client_name)
            for rep in client_reps:
                report = Report(str(rep[1]), str(rep[2]), client, str(rep[3]))
                client.add_report(report)
            for camp in client_camps:
                campaign = Campaign(camp[2], camp[3], camp[4])
                for sms in client_smss:
                    if (sms[1] == camp[1]):
                        subgr_name, subgr_subs = self.gateway.get_subgroup_by_id(sms[4])
                        subgr = Subgroup(subgr_name)
                        for sub in subgr_subs:
                            sub_ob = Subscriber(str(sub[1]), str(sub[2]))
                            subgr.add_subscriber(sub_ob)
                        sms_ob = Sms(str(sms[2]), str(sms[3]), subgr)
                        campaign.add_sms(sms_ob)
                client.add_campaign(campaign)
            return client


    def assemble_subgroup_from_rows(self, subgr_id):
        subgr_name, subgr_subs = self.gateway.get_subgroup_by_id(subgr_id)
        subgroup = Subgroup(subgr_name)
        for sub in subgr_subs:
            print '!!!!'
            print sub[1], sub[2]
            subscriber = Subscriber(str(sub[1]), str(sub[2]))
            subgroup.add_subscriber(subscriber)
        return subgroup

    #______________________#
    #___Authentification___#
    #______________________#

    def check_user(self, login, passwd):
        return_value = self.gateway.check_user(login, passwd)
        if (return_value == 1):
            user_type = 'ADMIN'
        elif (return_value == 2):
            user_type = 'CLIENT_ADM'
        elif (return_value == 3):
            user_type = 'SMS_ADM'
        elif (return_value == 4):
            user_type = 'SUB_ADM'
        else:
            user_type = 'NOT_EXISTS'
        return user_type

    #______________________#
    #_________Admin________#
    #______________________#

    def get_user_by_fio(self, fio):
        admins = self.gateway.get_user_by_fio(fio)
        return admins

    def add_user(self, fio, login, passwd, roles):
        print '!!!'
        print roles
        if roles != 1 and roles != 2 and roles != 3 and roles != 4:
            raise LogicError('error')
        self.gateway.add_user(fio, login, passwd, roles)

    def del_user(self, user_id):
        self.gateway.del_user(user_id)

    def change_roles_for_user(self, user_id, new_roles):
        self.gateway.change_roles_for_user(user_id, new_roles)

    #______________________#
    #_____Client_Admin_____#
    #______________________#

    def get_client(self, client_name):
        client_id = self.gateway.get_client_id_by_name(client_name)
        if (client_id == None):
            return None
        return self.assemble_client_from_rows(client_id)

    # I've just added this
    def get_client_id(self, client_name):
        client_id = self.gateway.get_client_id_by_name(client_name)
        if (client_id == None):
            return -1
        return client_id

    # Create new client
    def new_client(self, client_name):
        client = Client(client_name)
        self.gateway.add_client(client_name)
        return client # Returns just client

    # Change client name
    def change_name_client(self, client_id, new_name):
        client = self.assemble_client_from_rows(client_id)
        client.change_name(new_name)
        self.gateway.update_client(client_id, client)

    def new_report(self, first_date, second_date, client_id):
        client = self.assemble_client_from_rows(client_id)
        report = Report(first_date, second_date, client)
        client.add_report(report)
        self.gateway.update_client(client_id, client)

    #______________________#
    #______Sms_Admin_______#
    #______________________#

    def get_campaigns_by_client_id(self, client_id):
        client = self.assemble_client_from_rows(client_id)
        return self.campaigns_to_tuple(client.campaigns)

    def new_campaign(self, client_id):
        client = self.assemble_client_from_rows(client_id)
        campaign = Campaign()
        client.add_campaign(campaign)
        self.gateway.update_client(client_id, client)

    def new_sms(self, client_id, camp_id, sms_text, sms_time, subgroup_id):
        client = self.assemble_client_from_rows(client_id)
        print client.client_name
        #campaign = client.campaigns[camp_id]
    #    subgroup = gateway.get_subgroup_by_id(subgroup_id)
        subgr_name, subgr_subs = self.gateway.get_subgroup_by_id(subgroup_id)
        subgroup = Subgroup(subgr_name)
        for sub in subgr_subs:
            sub_ob = Subscriber(str(sub[1]), str(sub[2]))
            subgroup.add_subscriber(sub_ob)
        sms = Sms(sms_text, sms_time, subgroup)
        print sms.sms_time
        print sms.sms_text
        print sms.group.name
        client.campaigns[camp_id].add_sms(sms)
        sm = client.campaigns[camp_id].get_sms(0) #!!!!wtf
        print sm.sms_text
        #-----
        for camp in client.campaigns:
            for sms in camp.sms_s:
                print sms.sms_text
        self.gateway.update_client(client_id, client)

    # Function gets all sms
    def get_all_sms(self, client_id, camp_id):
        client = self.assemble_client_from_rows(client_id)
        campaign = client.get_campaign(camp_id)
        all_smss = self.smss_to_tuple(campaign)
        return all_smss

    def change_sms(self, client_id, camp_id, sms_id, new_text, new_time):

        client = self.assemble_client_from_rows(client_id)
        campaign = client.get_campaign(camp_id)
        sms = campaign.sms_s[sms_id]
        sms.change_text(new_text)
        sms.change_time(new_time)
        self.gateway.update_client(client_id, client)

    def send_sms(self, client_id, camp_id, sms_id):
        client = self.assemble_client_from_rows(client_id)
        campaign = client.get_campaign(camp_id)
        sms = campaign.sms_s[sms_id]
        text, time, subs = sms.get_send_info()
        return text, time, subs

    #______________________#
    #______Subs_Admin______#
    #______________________#

    def add_subgroup(self, subgroup_name):
        self.gateway.add_subgroup(subgroup_name)

    def get_all_subgroups(self):
        all_subgroups = self.gateway.get_all_subgroups()
        return all_subgroups #returns all subgroups' names and their id-s

    def get_subgroup_subs(self, subgr_id):
        subgr_name, subscribers = self.gateway.get_subgroup_by_id(subgr_id)
        return subscribers

#!!!!!
    def delete_subscriber(self, subgr_id, subscriber_name, subscriber_number):
        subgroup = self.assemble_subgroup_from_rows(subgr_id)
        for sub in subgroup.subs:
            if (subscriber_number == sub.number):
                subgroup.del_subscriber(sub)
        self.gateway.update_subgroup(subgr_id, subgroup)

    def change_subscriber(self, subgr_id, subscriber_name, subscriber_number, new_name, new_number):
        subgroup = self.assemble_subgroup_from_rows(subgr_id)
        for sub in subgroup.subs:
            if (subscriber_name == sub.get_name() and subscriber_number == sub.get_number()):
                sub.change_name(new_name)
                sub.change_number(new_number)
        self.gateway.update_subgroup(subgr_id, subgroup)

    def add_subscriber(self, subgr_id, subscriber_name, subscriber_number):
        subgroup = self.assemble_subgroup_from_rows(subgr_id)
        for subscr in subgroup.subs:
            if (subscriber_number == subscr.get_number()):
                return -1
        subscriber = Subscriber(subscriber_name, subscriber_number)
        subgroup.add_subscriber(subscriber)
        self.gateway.update_subgroup(subgr_id, subgroup)
