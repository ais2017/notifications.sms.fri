import unittest
from Code.classes.report import *
from Code.classes.client import *

class ReportTest(unittest.TestCase):
    def test_init(self):
        cl = Client('Client')
        rep = Report('2016-02-17', '2017-01-01', cl)
        self.assertEqual(rep.first, '2016-02-17')
        self.assertEqual(rep.second, '2017-01-01')

    def test_start_finish_time(self):
        cl = Client('Client')
        rep = Report('2016-02-17', '2017-01-01', cl)
        self.assertEqual(rep.start_time(), '2016-02-17')
        self.assertEqual(rep.end_time(), '2017-01-01')

    def test_report_making(self):
        camp = Campaign()
        cl = Client('Cl')
        camp = Campaign()
        cl.add_campaign(camp)
        camp.close_campaign()
        rep = Report('2016-02-17', '2018-01-01', cl)
        self.assertEqual(rep.rep_make(),
                         'During the period from 2016-02-17 til 2018-01-01 client Cl had 1 active campaigns')

    def test_type_error(self):
        cl = Client("TechServ")
        self.assertRaises(TypeError, rep = Report('2016-02-17', '2017-01-01', cl))
