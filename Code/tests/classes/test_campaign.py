import unittest
from Code.classes.campaign import *

class CampaignTest(unittest.TestCase):
    def test_init(self):
        camp = Campaign()
        self.assertAlmostEqual(camp.start_date, str(datetime.now())[:10])
        self.assertEqual(camp.end_date, 0)
        self.assertEqual(camp.status, True)
        self.assertEqual(camp.sms_s, list())

    def test_add_del_sms(self):
        s1 = Sms('Good morning, bro!\nGet free chicken legs every Wednesday at KFC!', str(datetime(2017, 10, 10, 0, 0))[:10], Subgroup('Group1'))
        s2 = Sms('Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], Subgroup('Group2'))
        camp = Campaign()
        camp.add_sms(s1)
        camp.add_sms(s2)
        self.assertEqual(len(camp.sms_s), 2)
        camp.del_sms(s2)
        self.assertEqual(len(camp.sms_s), 1)

    def test_close_campaign(self):
        s = Sms('Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], Subgroup('Group2'))
        camp = Campaign()
        camp.add_sms(s)
        self.assertEqual(camp.status, True)
        self.assertEqual(camp.end_date, 0)
        camp.close_campaign()
        self.assertEqual(camp.status, False)
        self.assertEqual(camp.end_date, str(datetime.now())[:10])

    def test_type_error(self):
        camp = Campaign()
        sms = Sms('Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], Subgroup('Group2'))
        self.assertRaises(TypeError, camp.add_sms(sms))
        self.assertRaises(TypeError, camp.del_sms(sms))