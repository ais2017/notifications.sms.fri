import unittest
from Code.classes.client import *
from Code.classes.report import *

class ClientTest(unittest.TestCase):
    def test_init(self):
        cl = Client('OOO TS')
        self.assertEqual(cl.client_name, 'OOO TS')
        self.assertEqual(cl.campaigns, list())
        self.assertEqual(cl.reports, list())

    def test_name_change(self):
        cl = Client('OOO TS')
        cl.change_name('OOO Apple Inc')
        self.assertEqual(cl.client_name, 'OOO Apple Inc')

    def test_add_del_campaign(self):
        cl = Client('OOO GDP')
        camp1 = Campaign()
        camp2 = Campaign()
        cl.add_campaign(camp1)
        cl.add_campaign(camp2)
        self.assertEqual(len(cl.campaigns), 2)
        cl.del_campaign(camp1)
        self.assertEqual(len(cl.campaigns), 1)

    def test_add_del_report(self):
        cl = Client('GDP')
        rep1 = Report('2016-02-17', '2017-01-01', cl)
        rep2 = Report('2016-02-17', '2017-01-01', cl)
        cl.add_report(rep1)
        cl.add_report(rep2)
        self.assertEqual(len(cl.reports), 2)
        cl.del_report(rep2)
        self.assertEqual(len(cl.reports), 1)

    def test_get_campaign(self):
        cl = Client('GDP')
        camp1 = Campaign()
        camp2 = Campaign()
        cl.add_campaign(camp1)
        cl.add_campaign(camp2)
        camp = cl.get_campaign(1)
        self.assertEqual(camp, camp2)

    def test_get_report(self):
        cl = Client('GDP')
        rep1 = Report('2016-02-17', '2017-01-01', cl)
        rep2 = Report('2016-02-17', '2017-02-01', cl)
        cl.add_report(rep1)
        cl.add_report(rep2)
        rep = cl.get_report(1)
        self.assertEqual(rep.second, '2017-02-01')

    def test_print_campaigns(self):
        cl = Client('GDP')
        camp1 = Campaign()
        camp2 = Campaign()
        cl.add_campaign(camp1)
        cl.add_campaign(camp2)
        self.assertEqual(cl.print_campaigns(),
                         "1 campaign: started at " + str(datetime.now())[:10] +
                         " ; number of sms: 0\n2 campaign: started at " +
                         str(datetime.now())[:10] + " ; number of sms: 0\n")

    def test_type_error(self):
        self.assertRaises(TypeError, cl = Client("GDP"))
        cl = Client("GDP")
        self.assertRaises(TypeError, cl.change_name("Test"))
        camp = Campaign()
        self.assertRaises(TypeError, cl.add_campaign(camp))
        self.assertRaises(TypeError, cl.del_campaign(camp))
        rep = Report('2016-02-17', '2017-01-01', cl)
        self.assertRaises(TypeError, cl.add_report(rep))
        self.assertRaises(TypeError, cl.del_report(rep))
