import unittest
from Code.classes.sms import *
from datetime import datetime

class SmsTest(unittest.TestCase):
    def test_init(self):
        s1 = Sms('Good morning, bro!\nGet free chicken legs every Wednesday at KFC!',
                 str(datetime(2017, 10, 10, 0, 0))[:10], Subgroup('Group1'))
        self.assertEqual(s1.sms_text, 'Good morning, bro!\nGet free chicken legs every Wednesday at KFC!')
        self.assertEqual(s1.sms_time, str(datetime(2017, 10, 10, 0, 0))[:10])

    def test_text_change(self):
        s1 = Sms('Good morning, bro!\nGet free chicken legs every Wednesday at KFC!',
                 str(datetime(2017, 10, 10, 0, 0))[:10], Subgroup('Group1'))
        s1.change_text('Nope')
        self.assertEqual(s1.sms_text, 'Nope')

    def test_time_change(self):
        s1 = Sms('Good morning, bro!\nGet free chicken legs every Wednesday at KFC!',
                 str(datetime(2017, 10, 10, 0, 0))[:10], Subgroup('Group1'))
        s1.change_time('2017-01-10')
        self.assertEqual(s1.sms_time, '2017-01-10')

    def test_gets(self):
        s = Subgroup('Group2')
        sms = Sms('Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], s)
        self.assertEqual(sms.get_text(), 'Hey!\nWatch this: we2ddd.onion')
        self.assertEqual(sms.get_time(), '2017-10-11')
        self.assertEqual(sms.get_group(), s)
        sms2 = Sms('Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0)), s)
        self.assertEqual(sms.get_time(), '2017-10-11')

    def test_send(self):
        s = Subgroup('Group')
        sub1 = Subscriber('qwqw', '12231')
        sub2 = Subscriber('qssa', '32231')
        s.add_subscriber(sub1)
        s.add_subscriber(sub2)
        sms = Sms('Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], s)
        self.assertEqual(sms.get_send_info()[0], 'Hey!\nWatch this: we2ddd.onion')
        self.assertEqual(sms.get_send_info()[1], '2017-10-11')
        self.assertEqual(sms.get_send_info()[2][0], '12231')
        self.assertEqual(sms.get_send_info()[2][1], '32231')

    def test_type_error(self):
        self.assertRaises(TypeError, s1 = Sms('Good morning, bro!\nGet free chicken legs every Wednesday at KFC!',
                 str(datetime(2017, 10, 10, 0, 0))[:10], Subgroup('Group1')))
        s1 = Sms('Good morning, bro!\nGet free chicken legs every Wednesday at KFC!',
                 str(datetime(2017, 10, 10, 0, 0))[:10], Subgroup('Group1'))
        self.assertRaises(TypeError, s1.change_time('2017-10-11'))
        self.assertRaises(TypeError, s1.change_text('Hello!'))