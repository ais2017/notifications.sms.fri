import unittest
from Code.classes.subscriber import *

class SubscrberTest(unittest.TestCase):
    def test_init(self):
        sub = Subscriber('Jack Morrow', '89092218277')
        self.assertEqual(sub.name, 'Jack Morrow')
        self.assertEqual(sub.number, '89092218277')

    def test_changes(self):
        sub = Subscriber('Jack Morrow', '89092218277')
        sub.change_name('Sparrow Captain')
        sub.change_number('89092218278')
        self.assertEqual(sub.name, 'Sparrow Captain')
        self.assertEqual(sub.number, '89092218278')

    def test_getters(self):
        sub = Subscriber('Jack Morrow', '89092218277')
        self.assertEqual(sub.get_name(), 'Jack Morrow')
        self.assertEqual(sub.get_number(), '89092218277')

    def test_type_error(self):
        self.assertRaises(TypeError, sub = Subscriber('Jack Morrow', '89092218277'))
        sub = Subscriber('Jack Morrow', '89092218277')
        self.assertRaises(TypeError, sub.change_name('Test'))
        self.assertRaises(TypeError, sub.change_number('100'))
