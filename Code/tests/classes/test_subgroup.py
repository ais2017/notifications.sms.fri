import unittest
from Code.classes.subgroup import *

class SubgroupTest(unittest.TestCase):
    def test_init(self):
        sgrp = Subgroup('Group1')
        self.assertEqual(sgrp.name, 'Group1')
        self.assertEqual(sgrp.subs, list())

    def test_name_change(self):
        sgrp = Subgroup('Group2')
        sgrp.change_name('Group3')
        self.assertEqual(sgrp.name,'Group3')

    def test_add_del_sub(self):
        sgrp = Subgroup('Group1')
        sub1 = Subscriber('FIO', '88005553535')
        sub2 = Subscriber('fio', '89991392344')
        sgrp.add_subscriber(sub1)
        sgrp.add_subscriber(sub2)
        self.assertEqual(len(sgrp.subs), 2)
        sgrp.del_subscriber(sub2)
        self.assertEqual(len(sgrp.subs), 1)

    def test_type_error(self):
        self.assertRaises(TypeError, sgrp = Subgroup('Group1'))
        sgrp = Subgroup('Group1')
        sub = Subscriber('Test', '100')
        self.assertRaises(TypeError, sgrp.add_subscriber(sub))
        self.assertRaises(TypeError, sgrp.del_subscriber(sub))
        self.assertRaises(TypeError, sgrp.change_name('New Group'))
