import unittest
from Code.use_cases.Use_case import *

class AssembleTest(unittest.TestCase):
    def test_assemble_client(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.gateway.clients.append('Qwe')
        uc.gateway.campaigns.append([0, 0, '2017-10-10', '0', True])
        uc.gateway.subgroups.append('Men')
        uc.gateway.smss.append([0, 0, 'Text', '2017-11-11', 0])
        client = uc.assemble_client_from_rows(0)
        self.assertEqual(client.client_name, 'Qwe')
        self.assertEqual(client.campaigns[0].start_date, '2017-10-10')
        self.assertEqual(client.campaigns[0].end_date, '0')
        self.assertEqual(client.campaigns[0].status, True)
        self.assertEqual(client.campaigns[0].sms_s[0].sms_text, 'Text')
        self.assertEqual(client.campaigns[0].sms_s[0].sms_time, '2017-11-11')
        self.assertEqual(client.campaigns[0].sms_s[0].group.name, 'Men')

    def test_assemble_subgroup(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.gateway.subgroups.append('Men')
        uc.gateway.subscribers.append([0, 'Name', 'Number'])
        uc.gateway.subscribers.append([0, 'Name2', 'Number2'])
        subgroup = uc.assemble_subgroup_from_rows(0)
        self.assertEqual(subgroup.name, 'Men')
        self.assertEqual(subgroup.subs[0].name, 'Name')
        self.assertEqual(subgroup.subs[0].number, 'Number')
        self.assertEqual(subgroup.subs[1].name, 'Name2')
        self.assertEqual(subgroup.subs[1].number, 'Number2')