import unittest
from Code.use_cases.Use_case import *


class SubgrAdminTest(unittest.TestCase):
    def test_add_subgroup(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.add_subgroup('Women after 40')
        self.assertEqual(len(uc.gateway.subgroups), 1)
        self.assertEqual(uc.gateway.subgroups[0], 'Women after 40')
        self.assertEqual(uc.gateway.subgroups[0], 'Women after 40')
        uc.add_subgroup('Men after 30')
        self.assertEqual(len(uc.gateway.subgroups), 2)
        self.assertEqual(uc.gateway.subgroups[1], 'Men after 30')

    def test_get_all_subgroups(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.add_subgroup('Women after 40')
        uc.add_subgroup('Men after 30')
        self.assertEqual(len(uc.gateway.subgroups), 2)
        self.assertEqual(uc.get_all_subgroups()[0][0], 0)
        self.assertEqual(uc.get_all_subgroups()[1][0], 1)
        self.assertEqual(uc.get_all_subgroups()[0][1], 'Women after 40')
        self.assertEqual(uc.get_all_subgroups()[1][1], 'Men after 30')

    def test_add_delete_and_get_subscriber(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.add_subgroup('Women after 40')
        uc.add_subscriber(0, 'Ivanov Petr', '89175336283')
        self.assertEqual(uc.get_subgroup_subs(0)[0][1], 'Ivanov Petr')
        self.assertEqual(uc.get_subgroup_subs(0)[0][2], '89175336283')
        uc.add_subscriber(0, 'Petrov Vlentin', '89192661263')
        self.assertEqual(uc.get_subgroup_subs(0)[1][1], 'Petrov Vlentin')
        self.assertEqual(uc.get_subgroup_subs(0)[1][2], '89192661263')
        self.assertEqual(len(uc.get_subgroup_subs(0)), 2)
        uc.add_subscriber(0, 'Bksaa Qjqjwq', '89192661263') # Shouldn't get anything
        self.assertEqual(len(uc.get_subgroup_subs(0)), 2)
        uc.delete_subscriber(0, 'Petrov Vlentin', '89192661263')
        self.assertEqual(len(uc.get_subgroup_subs(0)), 1)

    def test_change_subscriber(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.add_subgroup('Women after 40')
        uc.add_subscriber(0, 'Ivanov Petr', '89175336283')
        self.assertEqual(uc.get_subgroup_subs(0)[0][1], 'Ivanov Petr')
        self.assertEqual(uc.get_subgroup_subs(0)[0][2], '89175336283')
        uc.change_subscriber(0, 'Ivanov Petr', '89175336283', 'Ivanov Petr', '88005552525')
        self.assertEqual(uc.get_subgroup_subs(0)[0][1], 'Ivanov Petr')
        self.assertEqual(uc.get_subgroup_subs(0)[0][2], '88005552525')
        uc.change_subscriber(0, 'Ivanov Petr', '88005552525', 'Petrov Ivan', '86627772222')
        self.assertEqual(uc.get_subgroup_subs(0)[0][1], 'Petrov Ivan')
        self.assertEqual(uc.get_subgroup_subs(0)[0][2], '86627772222')
