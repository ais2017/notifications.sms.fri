import unittest
from Code.use_cases.Use_case import *

class UserAdminTest(unittest.TestCase):
    def test_radd_and_check_user(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.users), 0)
        uc.add_user('Titov Kirill', 'ktitov', 'qwerty', 1)
        self.assertEqual(len(uc.gateway.users), 1)
        self.assertEqual(uc.check_user('ktitov', 'qwerty'), 'ADMIN')
        self.assertEqual(uc.check_user('kktitov', 'qwerty'), 'NOT_EXISTS')
        self.assertEqual(uc.check_user('ktitov', 'qwert'), 'NOT_EXISTS')
        self.assertEqual(len(uc.get_user_by_fio('Titov Kirill')), 1)
        uc.add_user('Titov Kirill', 'admin', 'admin', 1)
        self.assertEqual(len(uc.get_user_by_fio('Titov Kirill')), 2)
        self.assertEqual(uc.get_user_by_fio('Titov Kirill')[0][1], 'Titov Kirill')

    def test_del_user(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.add_user('sd', 'sd', 'ds', 2)
        uc.add_user('qw', 'qw', 'qw', 4)
        self.assertEqual(len(uc.gateway.users), 2) #id-s 0 and 1
        uc.del_user(0)
        self.assertEqual(len(uc.gateway.users), 1) #id 1
        self.assertEqual(uc.gateway.users[0][0], 1)
        uc.add_user('Admin Admin', 'admin', 'admin', 2) #id-s 1 and 2
        self.assertEqual(len(uc.gateway.users), 2)
        self.assertEqual(uc.gateway.users[1][0], 2)
        uc.del_user(1) #id 2
        self.assertEqual(len(uc.gateway.users), 1)
        uc.add_user('Titov Kirill', 'ktitov', 'qwerty', 1) #id-s 2 and 3
        self.assertEqual(uc.gateway.users[1][0], 3)

    def test_change_roles(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.add_user('User User', 'user', 'user', 3)
        self.assertEqual(uc.gateway.users[0][4], 3) #role
        self.assertEqual(uc.gateway.users[0][0], 0) #id
        uc.change_roles_for_user(0, 2)
        self.assertEqual(uc.gateway.users[0][4], 2)
