import unittest
from Code.use_cases.Use_case import *

class SmsAdminTest(unittest.TestCase):
    def test_get_campaigns_by_client_name(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.clients), 0)
        uc.new_client('GDP')
        uc.new_campaign(0)
        uc.new_campaign(0)
        self.assertEqual(uc.get_campaigns_by_client_id(0)[0][0], 0)
        self.assertEqual(uc.get_campaigns_by_client_id(0)[0][1], str(datetime.now())[:10])
        self.assertEqual(uc.get_campaigns_by_client_id(0)[1][0], 1)

    def test_new_campaign(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.clients), 0)
        uc.new_client('GDP')
        uc.new_campaign(0)
        cl = uc.get_client('GDP')
        self.assertEqual(len(cl.campaigns), 1)
        self.assertEqual(cl.campaigns[0].start_date, str(datetime.now())[:10])
        self.assertEqual(cl.campaigns[0].end_date, 0)
        self.assertEqual(len(cl.campaigns[0].sms_s), 0)

    def test_new_get_all_sms(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.clients), 0)
        uc.new_client('GDP')
        uc.new_campaign(0)
        uc.add_subgroup('Men under 30')
        uc.new_sms(0, 0, 'Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], 0)
        cl = uc.get_client('GDP')
        self.assertEqual(cl.campaigns[0].sms_s[0].sms_text, 'Hey!\nWatch this: we2ddd.onion')
        self.assertEqual(cl.campaigns[0].sms_s[0].sms_time, str(datetime(2017, 10, 11, 0, 0))[:10])
        uc.add_subgroup('Men under 20')
        uc.new_sms(0, 0, 'Good morning, bro!\nGet free chicken legs every Wednesday at KFC!',
                 str(datetime(2017, 11, 10, 0, 0))[:10], 1)
        self.assertEqual(len(uc.get_all_sms(0, 0)), 2)
        self.assertEqual(uc.get_all_sms(0, 0)[0][1], str(datetime(2017, 10, 11, 0, 0))[:10])
        self.assertEqual(uc.get_all_sms(0, 0)[0][2], 'Hey!\nWatch this: we2ddd.onion')
        self.assertEqual(uc.get_all_sms(0, 0)[0][3], 'Men under 30')
        self.assertEqual(uc.get_all_sms(0, 0)[1][1], str(datetime(2017, 11, 10, 0, 0))[:10])
        self.assertEqual(uc.get_all_sms(0, 0)[1][2], 'Good morning, bro!\nGet free chicken legs every Wednesday at KFC!')
        self.assertEqual(uc.get_all_sms(0, 0)[1][3], 'Men under 20')
    
    def test_change_sms(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.new_client('GDP')
        uc.new_campaign(0)
        uc.add_subgroup('Men under 30')
        uc.new_sms(0, 0, 'Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], 0)
        uc.change_sms(0, 0, 0, 'None\n', str(datetime(2018, 01, 01, 0, 0))[:10])
        self.assertEqual(uc.get_all_sms(0, 0)[0][1], str(datetime(2018, 01, 01, 0, 0))[:10])
        self.assertEqual(uc.get_all_sms(0, 0)[0][2], 'None\n')
        self.assertEqual(uc.get_all_sms(0, 0)[0][3], 'Men under 30')

    def test_send_sms(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.new_client('GDP')
        uc.new_campaign(0)
        uc.add_subgroup('Men under 30')
        uc.add_subscriber(0, 'Test', '800')
        uc.add_subscriber(0, 'Test2', '900')
        uc.new_sms(0, 0, 'Hey!\nWatch this: we2ddd.onion', str(datetime(2017, 10, 11, 0, 0))[:10], 0)
        self.assertEqual(uc.send_sms(0, 0, 0)[0], 'Hey!\nWatch this: we2ddd.onion')
        self.assertEqual(uc.send_sms(0, 0, 0)[1], str(datetime(2017, 10, 11, 0, 0))[:10])
        self.assertEqual(uc.send_sms(0, 0, 0)[2][0], '800')
        self.assertEqual(uc.send_sms(0, 0, 0)[2][1], '900')
