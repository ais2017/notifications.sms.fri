import unittest
from Code.use_cases.Use_case import *

class ClientAdminTest(unittest.TestCase):
    def test_new_client(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        uc.new_client('GDP')
        self.assertEqual(uc.gateway.clients[0],'GDP')
        uc.new_client('QWwe')
        self.assertEqual(uc.gateway.clients[1], 'QWwe')
        self.assertEqual(len(uc.gateway.clients), 2)

    def test_get_client(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.clients), 0)
        uc.new_client('GDP')
        cl = uc.get_client('GDP')
        self.assertEqual(cl.client_name, 'GDP')

    def test_change_client_name(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.clients), 0)
        uc.new_client('GDP')
        self.assertEqual(uc.gateway.clients[0], 'GDP')
        uc.change_name_client(0, 'AAA')
        self.assertEqual(uc.gateway.clients[0], 'AAA')

    def test_new_report(self):
        uc = UseCases(Gateway())
        uc.gateway.flush_storage()
        self.assertEqual(len(uc.gateway.clients), 0)
        uc.new_client('GDP')
        uc.new_campaign(0)
        uc.new_report('2017-01-01', '2018-01-01', 0)
        self.assertEqual(uc.gateway.reports[0][3],
                         'During the period from 2017-01-01 til 2018-01-01 client GDP had 1 active campaigns')